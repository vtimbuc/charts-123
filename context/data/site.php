<?php

  $data = [
    'title' => 'Site Title',
    'description' => '',
    'favicon' => 'img/favicon.png',
    'base_url' => $site['base_url'],
    'url' => $site['url'],
    'template' => [
      'url' => $site['template_url']
    ]
  ];

  return $data;
